import cats.effect.IO
import io.circe.Json
import org.http4s.HttpRoutes
import io.circe.generic.auto._
import org.http4s.circe.CirceEntityCodec._
import org.http4s.dsl.Http4sDsl

object TestService {

  type Message = String
  private def errorBody(message: Message) = Json.obj(
    ("message", Json.fromString(message))
  )

  def routes(): HttpRoutes[IO] = {

    val dsl = new Http4sDsl[IO]{}
    import dsl._

    HttpRoutes.of[IO] {
      case req @ POST -> Root / "test" =>
        Ok("test")
    }
  }

}