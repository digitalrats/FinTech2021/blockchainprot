name := "blockchainprot"

version := "0.1"

scalaVersion := "2.13.6"
libraryDependencies += "org.http4s" %% "http4s-dsl" % "1.0-234-d1a2b53"
libraryDependencies += "org.http4s" %% "http4s-circe" % "1.0-234-d1a2b53"
libraryDependencies += "org.http4s" %% "http4s-blaze-server" % "1.0-234-d1a2b53"
libraryDependencies += "org.http4s" %% "http4s-core" % "1.0-234-d1a2b53"
libraryDependencies += "io.circe" %% "circe-generic" % "0.15.0-M1"
libraryDependencies += "io.circe" %% "circe-core" % "0.15.0-M1"
libraryDependencies += "io.circe" %% "circe-parser" % "0.15.0-M1"